
public class AuswahlBeilspiele {

	public static void main(String[] args) {
	
	int z1 = 5;
	int z2 = 7;
	
	int erg1 = min(z1,z2);
	int erg2 = max(z1,z2);
	
	System.out.println("Ergebnis :" + erg1);
	System.out.println("Ergebnis :" + erg2);
	
	}
	
	public static int min(int zahl1,int zahl2) {
		int erg1;
		if (zahl1 < zahl2) {
			erg1 = zahl1;
		}
		else {
			erg1 = zahl2;
		}
		return erg1;
	}
	
	public static int max(int zahl1,int zahl2) {
		int erg2;
		if (zahl1 > zahl2) {
			erg2 = zahl1;
		}
		else {
			erg2 = zahl2;
		}
		return erg2;
	}
}
