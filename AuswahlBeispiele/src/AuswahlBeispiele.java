
public class AuswahlBeispiele {

	public static void main(String[] args) {
		
		double z1 = 2.5;
		double z2 = 7.2;
		double z3 = 3.6;
		
		double erg = max2(2.5,7.2,3.6);  //7.2
		
		System.out.println("Ergebnis :" + erg);
		
		}
		
		public static double max2(double zahl1,double zahl2,double zahl3) {
			double erg;
			if (zahl1 > zahl2 && zahl1 > zahl3) {
				erg = zahl1;
			}
			else {
				if (zahl2 > zahl3) {
					erg = zahl2;
				}
				else {
					erg = zahl3;
				}
				}
			return erg;
		}

}
