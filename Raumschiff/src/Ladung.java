
public class Ladung {

	private String bezeichnung;
	private int menge;

	// Konstruktoren

	public Ladung() {
		this.bezeichnung = "Unbekannt";
		this.menge = 0;
	}

	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	// Getter und Setter

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setMenge(int menge) {
		this.menge = menge;
	}

	public int getMenge() {
		return menge;
	}

}
