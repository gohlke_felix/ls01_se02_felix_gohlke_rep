import java.util.ArrayList;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	

	public Raumschiff() {
		this.photonentorpedoAnzahl = 0;
		this.energieversorgungInProzent = 0;
		this.schildeInProzent = 0;
		this.huelleInProzent = 0;
		this.lebenserhaltungssystemeInProzent = 0;
		this.androidenAnzahl = 0;
		this.schiffsname = "Unbekannt";
		this.broadcastKommunikator = "Leer" ;
	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname, ArrayList<String> broadcastKommunikator) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		this.broadcastKommunikator = broadcastKommunikator;
	}

	// Photonentorpedos

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	// Energieversorgung in Prozent

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	// Schilde in Prozent

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	// H�lle in Prozent

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	// Lebenserhaltungssysteme in Prozent

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	// Androidenanzahl

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	// Schiffsname

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	// Ladung hinzuf�gen
	public void addLadung(Ladung neueLadung) {
	}

	// Photonentorpedos schiessen
	public void photonentorpedosSchiessen(Raumschiff r) {
		
	}

	// Phaserkanonen schiessen
	public void phaserkanoneSchiessen(Raumschiff r) {
	}

	// Treffer
	private void treffer(Raumschiff r) {
		
	}

	// Nachricht an Alle
	public void nachrichtAnAlle(String message) {
		this.broadcastKommunikator = message;
	}

	// Logbucheintr�ge zur�ckgeben
	public String eintraegeLogbuchZurueckgeben() {
		return broadcastKommunikator;
	}

	// Photonentorpedos laden
	public void photonentorpedosLaden(int anzahlTorpedos) {
		this.photonentorpedoAnzahl = anzahlTorpedos;
	}

	// Reparatur durchf�hren
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlDroiden) {
		this.schildeInProzent = schutzschilde;
		this.energieversorgungInProzent = energieversorgung;
		this.huelleInProzent = schiffshuelle;
		this.androidenAnzahl = anzahlDroiden;
	}

	// Zustand Raumschiff
	public void zustandRaumschiff() {
	}

	// Ladungsverzeichnis ausgeben
	public void ladungsverzeichnisAusgeben() {
	}

	// Ladungsverzeichnis aufr�umen
	public void ladungsverzeichnisAufraeumen() {
	}
}
