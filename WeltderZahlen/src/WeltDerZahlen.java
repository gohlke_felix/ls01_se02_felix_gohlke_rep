/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8 ;
    
    // Anzahl der Sterne in unserer Milchstraße
    int anzahlSterne = (int) 3E11 ;
    
    // Wie viele Einwohner hat Berlin?
    float bewohnerBerlin = 3800000 ;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    short alterTage = 6708 ;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    long gewichtKilogramm = 190000 ;  
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
    long flaecheGroessteLand = 17000000 ;
    
    // Wie groß ist das kleinste Land der Erde?
    int flaecheKleinsteLand = 2 ; 
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne in unserer Milchstraße: " + anzahlSterne);
    System.out.println("Anzahl Einwohner hat Berlin: " + bewohnerBerlin);
    System.out.println("Alter in Tage: " + alterTage);
    System.out.println("Wie viel wiegt das schwerste Tier der Welt? " + gewichtKilogramm);
    System.out.println("wie viele km² das größte Land er Erde hat? " + flaecheGroessteLand);
    System.out.println("Wie groß ist das kleinste Land der Erde? " + flaecheKleinsteLand);
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

